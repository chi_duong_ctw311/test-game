import express from 'express'
const PORT = process.env.PORT  || 3003
const app = express()
import userRouter from './routes/userRouter.js'

app.get('/',(req,res)=>{
    res.send("succes")
})

app.use('/user',userRouter)
app.listen(PORT,()=>{
    console.log(`listen in ${PORT}`)
})